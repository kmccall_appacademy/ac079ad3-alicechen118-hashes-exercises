# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hsh = Hash.new
  str.split(" ").each do |word|
    hsh[word] = word.length
  end
  hsh
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted = hash.sort_by { |key, value| value }
  sorted[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hsh = Hash.new
  word.each_char do |char|
    hsh[char] = word.count(char)
  end
  hsh
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = Hash.new
  arr.each { |el| hsh[el] = arr.count(el) }
  uniq_array = []
  hsh.each_key { |key| uniq_array << key }
  uniq_array
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_hash = { even: 0, odd: 0 }
  numbers.each do |number|
    even_odd_hash[:even] += 1 if number.even?
    even_odd_hash[:odd] += 1 if number.odd?
  end
  even_odd_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_hash = { "a" => 0, "e" => 0, "i" => 0, "o" => 0, "u" => 0 }
  string.each_char do |char|
    if vowel_hash.has_key?(char)
      vowel_hash[char] += 1
    end
  end
  most_common_value = vowel_hash.sort_by { |key, value| value }[-1][1]
  if vowel_hash.values.count(most_common_value) > 1
    tied = vowel_hash.select { |key, value| value == most_common_value }
    tied.sort_by { |key, value| key }[0][0]
  else
    vowel_hash.sort_by { |key, value| value }[-1][0]
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half_bdays = []
  students.each do |key, value|
    second_half_bdays << key if value > 6
  end
  combinations = []
  second_half_bdays.each_index do |idx1|
    ((idx1 + 1)...second_half_bdays.length).each do |idx2|
      combinations << [second_half_bdays[idx1], second_half_bdays[idx2]]
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = Hash.new
  specimens.each { |specimen| species[specimen] = specimens.count(specimen) }
  number_of_species = species.keys.length
  smallest_population_size = species.values.min
  largest_population_size = species.values.max
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)
  print normal_count
  print vandalized_count
  vandalized_count.all? do |key, value|
    normal_count[key] >= value
  end
end

def character_count(str)
  punctuation = %w(, . ' ? ! ; : )
  count = Hash.new
  str.downcase.each_char do |char|
    if !punctuation.include?(char) && char != " "
      count[char] = str.chars.count(char)
    end
  end
  count
end
